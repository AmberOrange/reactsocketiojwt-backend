# React project
### Using SocketIO, JWT and express.js

This project has the front-end compiled into the static folder.

If you wish to see the front-end React source code, you can find it here:

https://gitlab.com/AmberOrange/reactsocketiojwt-frontend

Simply write 'npm start' to start server at port 4000