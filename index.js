const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
const socketioJwt = require("socketio-jwt");
const jwt = require("jsonwebtoken");

const SECRET_KEY = "secretsauce";

app.use(express.json());

const unwrap = ({ name, colour, avatar }) => ({ name, colour, avatar });

let users = [];
let tempi = 0;

app.use(express.static('static'));

function generateAccessToken(username) {
  // expires after half and hour (1800 seconds = 30 minutes)
  return jwt.sign(username, SECRET_KEY /*, { expiresIn: '1800s' }*/);
}

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/login", (req, res) => {
  res.sendFile(__dirname + "/static/index.html");
});

app.get("/settings", (req, res) => {
  res.sendFile(__dirname + "/static/index.html");
});

app.post("/user", (req, res) => {
  let user;
  let token = null;
  try {
    var decoded = jwt.verify(req.body.token, SECRET_KEY);
    console.log("Received token: " + req.body.token)

    if ((user = users.find((u) => u.name === decoded.name)) !== undefined) {
      user.avatar = req.body.settings.avatar;
      user.colour = req.body.settings.colour;
      token = generateAccessToken({
        name: user.name,
        colour: user.colour,
        avatar: user.avatar,
      });
    } else {
      res.statusCode = 400;
      token = { message: "Something happened" };
    }
  } catch (err) {
    res.statusCode = 400;
    token = { message: "Invalid token" };
  }

  console.log("Sending back token: " + token);
  console.log("Or message: " + token.message);
  res.json(token);
});

app.get("/user", (req, res) => {
  console.log(
    `Username: ${req.query.username}, Password: ${req.query.password}`
  );
  let user;
  let token = null;
  if ((user = users.find((u) => u.name === req.query.username)) !== undefined) {
    if (user.password === req.query.password) {
      token = generateAccessToken({
        name: user.name,
        colour: user.colour,
        avatar: user.avatar,
      });
    } else {
      res.statusCode = 400;
      token = { message: "Incorrect password" };
    }
  } else {
    user = {
      name: req.query.username,
      password: req.query.password,
      colour: "black",
      avatar:
        "https://icon-library.com/images/free-user-icon/free-user-icon-8.jpg",
    };
    users.push(user);
    token = generateAccessToken(unwrap(user));
  }

  console.log("Sending back token: " + token);
  res.json(token);
});

// Authentication step
io.sockets
  .on(
    "connection",
    socketioJwt.authorize({
      secret: SECRET_KEY,
      timeout: 15000, // 15 seconds to send the authentication message
    })
  )
  .on("authenticated", (socket) => {
    //this socket is authenticated, we are good to handle more events from it.
    console.log(`hello! ${socket.decoded_token.name}`);
    io.emit("new user", unwrap(socket.decoded_token));

    socket.on("user update", (user) => {
      console.log("user update " + user.name);
      io.emit("user update", user);
    });

    socket.on("disconnect", () => {
      console.log(socket.decoded_token.name + " disconnected");
      io.emit("disconnect", socket.decoded_token.name);
    });
    socket.on("chat message", (msg) => {
      console.log("message: " + msg);
      io.emit("chat message", msg);
    });
  });

http.listen(4000, () => {
  console.log("listening on *:4000");
});
